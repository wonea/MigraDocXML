﻿namespace MigraDocPreviewer
{
    public class UserSettings
    {
        public string LayoutFile { get; set; }
        public string DataFile { get; set; }
        public string PreviewFile { get; set; }
    }
}
