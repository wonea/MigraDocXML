﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace MigraDocPreviewer
{
    public class SettingsManager<T> where T : class
    {
        private readonly string _filePath;

        public SettingsManager(string fileName)
        {
            _filePath = GetLocalFilePath(fileName);
        }

        private string GetLocalFilePath(string fileName)
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            return Path.Combine(appData, fileName);
        }

        public T Load() =>
            File.Exists(_filePath) ?
            JsonConvert.DeserializeObject<T>(File.ReadAllText(_filePath)) :
            null;

        public void Save(T settings)
        {
            string json = JsonConvert.SerializeObject(settings);
            File.WriteAllText(_filePath, json);
        }
    }
}
