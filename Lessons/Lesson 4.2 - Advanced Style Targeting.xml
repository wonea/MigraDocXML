<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 4.2 - Advanced Style Targeting</p>

        <p>So far all of the styles that we've created have used very simple targeting rules, just specifying what node type to be applied to, if we wanted to limit where the style was applied in any way, we had to rely on giving the style a name.</p>

        <p>In this lesson we look at restricting styles based on the element's parent node types.</p>

        <p>As a very simple example of this, look at the following style:</p>

        <p Style="Code">
            &lt;Style Target="TextFrame/Image"&gt;
            {Space(4)}&lt;Setters Width="2cm"/&gt;
            &lt;/Style&gt;
            
        </p>

        <p>This will set all images which are direct children of a TextFrame to be 2cm wide, any images which aren't the child of a TextFrame will be ignored by this style.</p>

        <p>This alone is not that useful though, since if an image is the child of a paragraph which is the child of a TextFrame, this style won't be applied.</p>

        
        
        <p Style="SubTitle">Wildcards</p>

        <p>As mentioned above, the example style shown above is very rigid in how it's be applied to elements. This is where wildcards come in to give us back some flexibility. The example below will apply a default style to all images that belong to tables:</p>

        <p Style="Code">
            &lt;Style Target="Table/*/Image"&gt;
            {Space(4)}&lt;Setters Width="2cm"/&gt;
            &lt;/Style&gt;

        </p>

        <p>Wildcards come in 2 flavours: single (_) and any (*).</p>

        <p>A <i>single</i> wildcard will stand in place for one single parent type. So <i>'Section/_/Paragraph'</i> could be <i>'Section/TextFrame/Paragraph'</i> but not <i>'Section/Table/Row/C0/Paragraph'</i>.</p>

        <p>An <i>any</i> wildcard can stand in place for any number of parent types, even zero. So <i>'Section/*/Paragraph'</i> could be <i>'Section/TextFrame/Paragraph'</i>, <i>'Section/Table/Row/C0/Paragraph'</i> or <i>'Section/Paragraph'</i>.</p>

        <p>Note, wildcards can <b>not</b> be used to start or end a style's target definition.</p>

        <p>It was mentioned in the previous lesson that when you use the <i>Format</i> attributes on tables, rows, columns, etc. that it implicitly creates a paragraph style within that element. Since columns don't actually contain any child elements themselves, they instead create a style on their parent table with an advanced target definition, something along the lines of this: <i>Document/Section/Table/Row/C1/*/Paragraph</i>. For this reason, it is strongly recommended to use <b>C0</b>, <b>C1</b> etc. instead of <b>Cell Index="0"</b>, because the latter won't be targeted by column styles.</p>
    

    
        <p Style="SubTitle">Filtering By Parent Named Styles</p>

        <p>Styles can also be limited to only apply to certain elements if one of the element's parents has a certain named style, this is done by following the parent type name with one or more style names space separated in parentheses. As an example, we can use this approach to easily create a table with alternating row colours:</p>

        <Style Target="Table" Name="AlternatingRows">
            <Setters/>
        </Style>
        
        <Style Target="Table(AlternatingRows)/Row">
            <Setters Shading.Color="{(Row.Index % 2 == 1) ? 'LightGray' : 'White'}"/>
        </Style>
            
        <Table Style="AlternatingRows" Borders.Color="Black">
            <Column Width="5cm"/>
            <Column Width="5cm"/>
        
            <Row C0="hello" C1="world"/>
            <Row C0="hello" C1="world"/>
            <Row C0="hello" C1="world"/>
            <Row C0="hello" C1="world"/>
        </Table>
    
        <p Style="Code">
            &lt;Style Target="Table" Name="AlternatingRows"&gt;
                {Space(4)}&lt;Setters/&gt;
            &lt;/Style&gt;
        
            &lt;Style Target="Table(AlternatingRows)/Row"&gt;
                {Space(4)}&lt;Setters Shading.Color="```{(Row.Index % 2 == 1) ? 'LightGray' : 'White'}```"/&gt;
            &lt;/Style&gt;
            
            &lt;Table Style="AlternatingRows" Borders.Color="Black"&gt;
                {Space(4)}&lt;Column Width="5cm"/&gt;
                {Space(4)}&lt;Column Width="5cm"/&gt;
        
                {Space(4)}&lt;Row C0="hello" C1="world"/&gt;
                {Space(4)}&lt;Row C0="hello" C1="world"/&gt;
                {Space(4)}&lt;Row C0="hello" C1="world"/&gt;
                {Space(4)}&lt;Row C0="hello" C1="world"/&gt;
            &lt;/Table&gt;

        </p>

        <p>In this example, the Table style 'AlternatingRows' doesn't actually perform any action itself, but then we define another style which applies only to the rows of tables which have the 'AlternatingRows' style which does the actual row colouring based on the modulus of the row index. To make use of this behaviour, we now just need to create a table which uses the 'AlternatingRows' style.</p>

        <p Style="SubTitle">Multi-Targetted Styles</p>

        <p>With the introduction of MigraDocXML v4.5.0 comes a new styling feature. Consider the following situation:</p>

        <p Style="Code">&lt;Table Borders.Color="Black"&gt;
            {Space(4)}&lt;Style Name="important" Target="p"&gt;
                {Space(8)}&lt;Setters Format.Font.Bold="true" Format.Font.Color="Red"/&gt;
            {Space(4)}&lt;/Style&gt;
            
            {Space(4)}&lt;Column Width="2cm"/>
            {Space(4)}&lt;Column Width="2cm"/>
            {Space(4)}&lt;Column Width="2cm" Style="important"/>
            
            {Space(4)}&lt;Row C0="a" C1="b" C2="c"/&gt;
            {Space(4)}&lt;C2="c"&gt;
                {Space(8)}&lt;C0 Style="important"&gt;a&lt;/C0&gt;
                {Space(8)}&lt;C1&gt;&lt;p Style="important"&gt;b&lt;/p&gt;&lt;/C1&gt;
            {Space(4)}&lt;/Row&gt;
            {Space(4)}&lt;Row Style="important" C0="a" C1="b" C2="c"/&gt;
            &lt;/Table&gt;
        </p>

        <p>It's pretty clear that the intention of the above example is to produce something like this:</p>

        <Table Borders.Color="Black">
            <Style Name="important" Target="p|Column|Row|Cell">
                <Setters Format.Font.Bold="true" Format.Font.Color="Red"/>
            </Style>

            <Column Width="2cm"/>
            <Column Width="2cm"/>
            <Column Width="2cm" Style="important"/>

            <Row C0="a" C1="b" C2="c"/>
            <Row C2="c">
                <C0 Style="important">a</C0>
                <C1><p Style="important">b</p></C1>
            </Row>
            <Row Style="important" C0="a" C1="b" C2="c"/>
        </Table>

        <p>But what we actually get is this:</p>

        <Table Borders.Color="Black">
            <Style Name="important" Target="p">
                <Setters Format.Font.Bold="true" Format.Font.Color="Red"/>
            </Style>

            <Column Width="2cm"/>
            <Column Width="2cm"/>
            <Column Width="2cm" Style="important"/>

            <Row C0="a" C1="b" C2="c"/>
            <Row C2="c">
                <C0 Style="important">a</C0>
                <C1>
                    <p Style="important">b</p>
                </C1>
            </Row>
            <Row Style="important" C0="a" C1="b" C2="c"/>
        </Table>

        <p>Of all the elements using the <b>important</b> style, only one of them is actually a paragraph, so the style gets ignored by all the others.</p>

        <PageBreak/>

        <p>One way to get around this would be to create the same style 4 times: for p, Column, Row &amp; Table. This is very wasteful though. A better solution is to set 4 different targets on our style:</p>

        <p Style="Code">{Space(4)}&lt;Style Name="important" Target="p|Column|Row|Cell"&gt;
                {Space(8)}&lt;Setters Format.Font.Bold="true" Format.Font.Color="Red"/&gt;
            {Space(4)}&lt;/Style&gt;
        </p>

        <p>Obviously though, we need to be careful that all the targets actually support the properties being set in the style.</p>

        <p>Additionally, all of the previous styling options covered can still be used when combining styles, for example we could do:</p>

        <p Style="Code">
            {Space(4)}&lt;Style Target="TextFrame(float)/*/p|C0/p|TextFrame(bottom)/*/p"&gt;
                {Space(8)}&lt;Setters Format.Font.Underline="Single"/&gt;
            {Space(4)}&lt;/Style&gt;
        </p>
    </Section>
</Document>