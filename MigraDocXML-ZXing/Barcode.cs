﻿using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using ZXing;

namespace MigraDocXML_ZXing
{
	public class Barcode : Shape
    {

        private MigraDoc.DocumentObjectModel.Shapes.Image _model;

        protected BarcodeWriter<System.DrawingCore.Bitmap> _writer;


        public Barcode()
        {
            _model = new MigraDoc.DocumentObjectModel.Shapes.Image();
            ShapeModel = _model;
            _writer = new BarcodeWriter<System.DrawingCore.Bitmap>();
            ParentSet += Barcode_ParentSet;
            FullyBuilt += OnFullyBuilt;
        }

        private void Barcode_ParentSet(object sender, EventArgs e)
        {
            DOMRelations.Relate(GetPresentableParent(), this);
            ApplyStyling();
        }

        public override void SetTextValue(string value)
        {
            Contents = value;
        }

        public override MigraDoc.DocumentObjectModel.DocumentObject GetModel()
        {
            return _model;
        }

        public MigraDoc.DocumentObjectModel.Shapes.Image GetImageModel()
        {
            return _model;
        }

        private static Random _random = new Random();

        private void OnFullyBuilt(object sender, EventArgs e)
        {
			if (Rotation != 0 && Rotation != 90 && Rotation != 180 && Rotation != 270)
				throw new InvalidOperationException("Error, barcode rotation angle must be zero or a multiple of 90");
            Bitmap bitmap = Bitmap.FromHbitmap(_writer.Write(Contents).GetHbitmap());
			if(Rotation == 0)
			{
				if (FlipX && FlipY)
					bitmap.RotateFlip(RotateFlipType.RotateNoneFlipXY);
				else if (FlipX)
					bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
				else if(FlipY)
					bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
			}
			else if (Rotation == 90)
			{
				if (FlipX && FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate90FlipXY);
				else if (FlipX)
					bitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
				else if (FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate90FlipY);
				else
					bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
			}
			else if(Rotation == 180)
			{
				if (FlipX && FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate180FlipXY);
				else if (FlipX)
					bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
				else if (FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate180FlipY);
				else
					bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
			}
			else if(Rotation == 270)
			{
				if (FlipX && FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate270FlipXY);
				else if (FlipX)
					bitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
				else if (FlipY)
					bitmap.RotateFlip(RotateFlipType.Rotate270FlipY);
				else
					bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
			}
            byte[] bmpBytes = new ImageConverter().ConvertTo(bitmap, typeof(byte[])) as byte[];
            _model.Name = "base64:" + Convert.ToBase64String(bmpBytes);
        }


        public string Contents { get; set; }

        public bool LockAspectRatio { get => _model.LockAspectRatio; set => _model.LockAspectRatio = value; }

        public double Resolution { get => _model.Resolution; set => _model.Resolution = value; }

        public double ScaleHeight { get => _model.ScaleHeight; set => _model.ScaleHeight = value; }

        public double ScaleWidth { get => _model.ScaleWidth; set => _model.ScaleWidth = value; }

        public string Format
        {
            get => _writer.Format.ToString();
            set => _writer.Format = (BarcodeFormat)Enum.Parse(typeof(BarcodeFormat), value);
        }

        public bool GS1Format { get => _writer.Options.GS1Format; set => _writer.Options.GS1Format = value; }
		
        public int PixelHeight { get => _writer.Options.Height; set => _writer.Options.Height = value; }

        public int BarcodeMargin { get => _writer.Options.Margin; set => _writer.Options.Margin = value; }

        public int PixelWidth { get => _writer.Options.Width; set => _writer.Options.Width = value; }

        public bool PureBarcode { get => _writer.Options.PureBarcode; set => _writer.Options.PureBarcode = value; }

		private int _rotation = 0;
		public int Rotation
		{
			get => _rotation;
			set => _rotation = ((value % 360) + 360) % 360;
		}

		public bool FlipX { get; set; }

		public bool FlipY { get; set; }
	}
}
