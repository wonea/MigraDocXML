﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ZXing;
using ZXing.Common;
using ZXing.Rendering;
using ZXing.ZKWeb.Rendering;

namespace MigraDocXML_ZXing
{
    public class Code128 : Barcode
    {

        private ZXing.OneD.Code128EncodingOptions _options;


        public Code128()
            : base()
        {
            _options = new ZXing.OneD.Code128EncodingOptions();
            _writer.Options = _options;
            _writer.Format = ZXing.BarcodeFormat.CODE_128;
        }


        public bool ForceCodesetB { get => _options.ForceCodesetB; set => _options.ForceCodesetB = value; }


		public override void SetTextValue(string value)
		{
			if(GS1Format)
			{
				List<KeyValuePair<string, string>> splits = null;
				//If we can't even succeed in splitting out the text into a GS1 compliant format, then treat it as not GS1
				try
				{
					splits = GS1.SplitReadableText(value);
				}
				catch
				{
					base.SetTextValue(value);
					return;
				}

				string gs1Text = GS1.BuildGS1Barcode(splits, (char)241);
				_writer.Renderer = new GS1Renderer(value);
				base.SetTextValue(gs1Text);
			}
			else
				base.SetTextValue(value);
		}


		/// <summary>
		/// Basis for this class taken from here:
		/// https://github.com/micjahn/ZXing.Net/issues/38
		/// </summary>
		internal class GS1Renderer : BitmapRenderer
		{
			/// <summary>
			/// Will contain a value used for number at the bottom of the barcode image
			/// </summary>
			public string FriendlyContent { get; set; }

			public GS1Renderer(string friendlyContent = null)
			{
				FriendlyContent = friendlyContent;
			}

			public new System.DrawingCore.Bitmap Render(BitMatrix matrix, BarcodeFormat format, string content, EncodingOptions options)
			{
				// If the FriendlyContent has been set, use that in place of the content parameter, otherwise default to content.
				content = (!string.IsNullOrWhiteSpace(FriendlyContent)) ? FriendlyContent : content;

                return base.Render(matrix, format, content, options);
			}
		}
	}
}
