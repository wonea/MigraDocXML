﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDocXML.DOM;

namespace MigraDocXML_ZXing
{
    public static class Setup
    {
        /// <summary>
        /// The folder location where barcode images are temporarily saved to
        /// </summary>
        [Obsolete("This property is no longer required since MigraDoc v1.50 allows for images to be added straight from memory as base64 strings")]
        public static string TempFolder { get; set; }


        public static void Run()
        {
            DOMTypes.Add<Barcode>("Barcode");
            DOMTypes.Add<AztecCode>("AztecCode");
            DOMTypes.Add<Code128>("Code128");
            DOMTypes.Add<DataMatrix>("DataMatrix");
            DOMTypes.Add<PDF417>("PDF417");
            DOMTypes.Add<QRCode>("QRCode");

            DOMRelations.Create<Cell, Barcode>((parent, child) => (parent as Cell).GetCellModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<Footer, Barcode>((parent, child) => (parent as Footer).GetFooterModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<Header, Barcode>((parent, child) => (parent as Header).GetHeaderModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<Hyperlink, Barcode>((parent, child) => (parent as Hyperlink).GetHyperlinkModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<Paragraph, Barcode>((parent, child) => (parent as Paragraph).GetParagraphModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<Section, Barcode>((parent, child) => (parent as Section).GetSectionModel().Add((child as Barcode).GetImageModel()));

            DOMRelations.Create<TextFrame, Barcode>((parent, child) => (parent as TextFrame).GetTextFrameModel().Add((child as Barcode).GetImageModel()));
        }
    }
}
