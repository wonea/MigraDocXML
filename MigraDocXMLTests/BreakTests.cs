﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class BreakTests
    {
        [Fact]
        public void BreakInLoop()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<ForEach Var=\"item\" In=\"Model\">" +
                            "<If Test=\"item > 2\">" +
                                "<Break/>" +
                            "</If>" +
                            "<p>{item}</p>" +
                        "</ForEach>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run(new[] { 1, 2, 3, 4, 5 });
            Section section = doc.Children.OfType<Section>().First();
            
            Assert.Single(section.Children);
            Assert.IsType<ForEach>(section.Children.First());
            Assert.Equal(2, section.GetSectionModel().Elements.Count);

            var paragraphs = section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().ToList();
            Assert.Equal("1", paragraphs[0].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal("2", paragraphs[1].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }

        [Fact]
        public void BreakOutLoop()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>1</p>" +
                        "<Break/>" +
                        "<p>2</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.GetSectionModel().Elements);

            var paragraphs = section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().ToList();
            Assert.Equal("1", paragraphs[0].Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }
    }
}
