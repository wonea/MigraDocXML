﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class ImageTests
    {
        [Fact]
        public void SimpleImage()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Image Name=\"testimg.png\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Image img = section.Children.OfType<Image>().First();
            
            Assert.Equal("testimg.png", img.GetImageModel().Name);
            Assert.Equal(section.GetSectionModel(), img.GetImageModel().Section);
        }
    }
}
