using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Linq;
using Xunit;

namespace MigraDocXMLTests
{
    public class SectionTests
    {
        [Fact]
        public void SingleSection()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.VerticalMargin=\"5cm\">" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(5.0, section.GetSectionModel().PageSetup.TopMargin.Centimeter);
            Assert.Equal(5.0, section.GetSectionModel().PageSetup.BottomMargin.Centimeter);

            Assert.Single(doc.GetDocumentModel().Sections);
            Assert.Equal(doc.GetDocumentModel(), section.GetSectionModel().Document);
        }


        [Fact]
        public void MultipleSections()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section PageSetup.Orientation=\"Landscape\">" +
                    "</Section>" +
                    "<Section PageSetup.DifferentFirstPageHeaderFooter=\"true\">" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section1 = doc.Children.OfType<Section>().First();
            Section section2 = doc.Children.OfType<Section>().Last();

            Assert.Equal(2, doc.Children.OfType<Section>().Count());

            Assert.Equal(MigraDoc.DocumentObjectModel.Orientation.Landscape, section1.GetSectionModel().PageSetup.Orientation);

            Assert.True(section2.GetSectionModel().PageSetup.DifferentFirstPageHeaderFooter);

            Assert.Equal(2, doc.GetDocumentModel().Sections.Count);
            Assert.Equal(doc.GetDocumentModel(), section1.GetSectionModel().Document);
            Assert.Equal(doc.GetDocumentModel(), section2.GetSectionModel().Document);
        }
    }
}
