﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class QuitTests
    {
        [Fact]
        public void Quit()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>hello</p>" +
                        "<Quit/>" +
                        "<p>goodbye</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Assert.Null(doc);
        }
    }
}
