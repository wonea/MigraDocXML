﻿using MigraDoc.DocumentObjectModel;
using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
	public class PdfXmlReaderTests
	{
		[Fact]
		public void SetIntFromAttribute()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "IntProp", "123");
			Assert.Equal(123, testDOM.IntProp);
		}

		[Fact]
		public void IntFromAttributeFail()
		{
			var testDOM = new TestDOM();
			Assert.Throws<ArgumentException>(() => PdfXmlReader.SetPropertyFromAttribute(testDOM, "IntProp", "ham"));
		}

		[Fact]
		public void SetNullIntFromAttribute1()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullIntProp", "123");
			Assert.Equal(123, testDOM.NullIntProp);
		}

		[Fact]
		public void SetNullIntFromAttribute2()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullIntProp", null);
			Assert.Null(testDOM.NullIntProp);
		}

		[Fact]
		public void NullIntFromAttributeFail()
		{
			var testDOM = new TestDOM();
			Assert.Throws<ArgumentException>(() => PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullIntProp", "ham"));
		}

		[Fact]
		public void SetStringFromAttribute()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "StringProp", "123");
			Assert.Equal("123", testDOM.StringProp);
		}

		[Fact]
		public void SetDoubleFromAttribute1()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "DoubleProp", "123.0");
			Assert.Equal(123.0, testDOM.DoubleProp);
		}

		[Fact]
		public void SetDoubleFromAttribute2()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "DoubleProp", "123");
			Assert.Equal(123.0, testDOM.DoubleProp);
		}

		[Fact]
		public void DoubleFromAttributeFail()
		{
			var testDOM = new TestDOM();
			Assert.Throws<ArgumentException>(() => PdfXmlReader.SetPropertyFromAttribute(testDOM, "DoubleProp", "ham"));
		}

		[Fact]
		public void SetNullDoubleFromAttribute1()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullDoubleProp", "123");
			Assert.Equal(123.0, testDOM.NullDoubleProp);
		}

		[Fact]
		public void SetNullDoubleFromAttribute2()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullDoubleProp", null);
			Assert.Null(testDOM.NullDoubleProp);
		}

		[Fact]
		public void SetNullDoubleFromAttribute3()
		{
			var testDOM = new TestDOM();
			PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullDoubleProp", "123.0");
			Assert.Equal(123.0, testDOM.NullDoubleProp);
		}

		[Fact]
		public void NullDoubleFromAttributeFail()
		{
			var testDOM = new TestDOM();
			Assert.Throws<ArgumentException>(() => PdfXmlReader.SetPropertyFromAttribute(testDOM, "NullDoubleProp", "ham"));
		}


		private class TestDOM : DOMElement
		{
			public int IntProp { get; set; }
			public int? NullIntProp { get; set; }
			
			public string StringProp { get; set; }

			public double DoubleProp { get; set; }
			public double? NullDoubleProp { get; set; }

			public override DocumentObject GetModel() => throw new NotImplementedException();
		}
	}
}
