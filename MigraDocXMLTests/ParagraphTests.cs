﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class ParagraphTests
    {
        [Fact]
        public void SimpleParagraph()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p Format.Font.Bold=\"true\">This is a test</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("This is a test", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.True(p.GetParagraphModel().Format.Font.Bold);
            Assert.Single(p.GetParagraphModel().Elements);

            Assert.Single(section.GetSectionModel().Elements);
            Assert.Equal(section.GetSectionModel(), p.GetParagraphModel().Section);
        }

        [Fact]
        public void XmlParagraph()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>&lt;?xml version=\"1.0\" encoding=\"utf-8\"?&gt;" +
                        "&lt;Document&gt;" +
                        "{Space(4)}&lt;Section/&gt;" +
                        "&lt;/Document&gt;</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                new string(Convert.ToChar(160), 4) + "<Section/>" +
                "</Document>", 
                p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
            );
        }

		[Fact]
		public void NullParagraphContent()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p>{null}</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Empty(
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>()
			);
		}

		[Fact]
		public void CorrectParagraphSpacing1()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			hello\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"hello",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing2()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			```{1 + 1}```\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"{1 + 1}",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing3()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			{1 + 1}\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"2",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing4()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			```{1 + 1}``` >>> {1 + 1}\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"{1 + 1} >>> 2",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing5()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			1 + 1 = {1 + 1}\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"1 + 1 = 2",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing6()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p> hello</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				" hello",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing7()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p>hello </p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"hello ",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing8()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p>  hello</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				" hello",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing9()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p>hello  </p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"hello ",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing10()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p>he  llo</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			Assert.Equal(
				"he llo",
				p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content
			);
		}

		[Fact]
		public void CorrectParagraphSpacing11()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			hello\r\n" +
				"			world\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();
			
			var texts = p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().ToList();
			Assert.Equal(2, texts.Count);
			Assert.Equal("hello\r", texts[0].Content);
			Assert.Equal("world", texts[1].Content);
		}

		[Fact]
		public void CorrectParagraphSpacing12()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
				"<Document>\r\n" +
				"	<Section>\r\n" +
				"		<p>\r\n" +
				"			```{1 + 1}``` >>> {1 + 1}\r\n" +
				"			```{2 + 2}``` >>> {2 + 2}\r\n" +
				"		</p>\r\n" +
				"	</Section>\r\n" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p = section.Children.OfType<Paragraph>().First();

			var texts = p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().ToList();
			Assert.Equal(2, texts.Count);
			Assert.Equal("{1 + 1} >>> 2\r", texts[0].Content);
			Assert.Equal("{2 + 2} >>> 4", texts[1].Content);
		}
	}
}
