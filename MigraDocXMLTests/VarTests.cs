﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class VarTests
    {
        [Fact]
        public void BracketedVar()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test=\"{2}\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.Children);
            Assert.IsType<Var>(section.Children.First());
            Assert.Equal(2, section.GetVariable("test"));
        }

        [Fact]
        public void UnbracketedVar()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test=\"2\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.Children);
            Assert.IsType<Var>(section.Children.First());
            Assert.Equal(2, section.GetVariable("test"));
        }

        [Fact]
        public void VarScopes()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Var test=\"1\"/>" +
                    "<Section>" +
                        "<Var test=\"2\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(1, doc.GetVariable("test"));
            Assert.Equal(2, section.GetVariable("test"));
        }

        [Fact]
        public void MultipleDefs()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test1=\"1\" test2=\"2\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            
            Assert.Equal(1, section.GetVariable("test1"));
            Assert.Equal(2, section.GetVariable("test2"));
        }
    }
}
