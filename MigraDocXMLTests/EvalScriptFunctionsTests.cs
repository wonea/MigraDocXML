﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
	public class EvalScriptFunctionsTests
	{
		[Fact]
		public void GetAreaTest()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p Tag=\"tag1\">Test 1</p>" +
						"<p Tag=\"tag2\">Test 2</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p1 = section.Children.OfType<Paragraph>().First();
			Paragraph p2 = section.Children.OfType<Paragraph>().Last();

			doc.AddGraphics();

			var p1Area = EvalScriptGraphicsFunctions.GetArea(new object[1] { p1 });
			var p2Area = EvalScriptGraphicsFunctions.GetArea(new object[1] { p2 });

			Assert.Equal(1, p1Area.Page);
			Assert.Equal(1, p2Area.Page);
			Assert.Equal(p1Area.Left, p2Area.Left);
			Assert.Equal(p1Area.Width, p2Area.Width);
			Assert.True(p1Area.Top.Points < p2Area.Top.Points);
			Assert.Equal(p1Area.Height, p2Area.Height);
		}

		[Fact]
		public void GetTaggedAreasTest()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p Tag=\"tag\">Test 1</p>" +
						"<p Tag=\"tag\">Test 2</p>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			Paragraph p1 = section.Children.OfType<Paragraph>().First();
			Paragraph p2 = section.Children.OfType<Paragraph>().Last();

			doc.AddGraphics();

			var areas = EvalScriptGraphicsFunctions.GetTaggedAreas(new object[2] { doc, "tag" });

			Assert.Equal(2, areas.Count);
			var p1Area = areas[0];
			var p2Area = areas[1];

			Assert.Equal(1, p1Area.Page);
			Assert.Equal(1, p2Area.Page);
			Assert.Equal(p1Area.Left, p2Area.Left);
			Assert.Equal(p1Area.Width, p2Area.Width);
			Assert.True(p1Area.Top.Points < p2Area.Top.Points);
			Assert.Equal(p1Area.Height, p2Area.Height);
		}

		[Fact]
		public void GetTextFrameTaggedAreas()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<p Tag=\"tag\">Test 1</p>" +
						"<TextFrame>" +
							"<p Tag=\"tag\">Test 2</p>" +
							"<p Tag=\"tag\">Test 3</p>" +
						"</TextFrame>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			TextFrame frame = section.Children.OfType<TextFrame>().First();
			Paragraph p1 = frame.Children.OfType<Paragraph>().First();
			Paragraph p2 = frame.Children.OfType<Paragraph>().Last();

			doc.AddGraphics();

			var areas = EvalScriptGraphicsFunctions.GetTaggedAreas(new object[2] { frame, "tag" });

			Assert.Equal(2, areas.Count);
			var p1Area = areas[0];
			var p2Area = areas[1];

			Assert.Equal(1, p1Area.Page);
			Assert.Equal(1, p2Area.Page);
			Assert.Equal(p1Area.Left, p2Area.Left);
			Assert.Equal(p1Area.Width, p2Area.Width);
			Assert.True(p1Area.Top.Points < p2Area.Top.Points);
			Assert.Equal(p1Area.Height, p2Area.Height);
		}
	}
}
