﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class IfTests
    {
        [Fact]
        public void SimpleIf()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<If Test=\"Model = 'hello'\">" +
                            "<p>Hello</p>" +
                        "</If>" +
                        "<If Test=\"Model = 'goodbye'\">" +
                            "<p>Goodbye</p>" +
                        "</If>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run("hello");
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(2, section.Children.Count());
            Assert.All(section.Children, x => Assert.IsType<If>(x));
            Assert.Single(section.GetSectionModel().Elements);

            Assert.Equal("Hello", section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().First().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }

        [Fact]
        public void IfElse()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<If Test=\"Model = 'howdy'\">" +
                            "<p>Howdy</p>" +
                        "</If>" +
                        "<Else>" +
                            "<p>Goodbye</p>" +
                        "</Else>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run("hello");
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(2, section.Children.Count());
            Assert.IsType<If>(section.Children.First());
            Assert.IsType<Else>(section.Children.Last());
            Assert.Single(section.GetSectionModel().Elements);

            Assert.Equal("Goodbye", section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().First().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }

        [Fact]
        public void IfElseIfElse()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<If Test=\"Model = 'howdy'\">" +
                            "<p>Howdy</p>" +
                        "</If>" +
                        "<ElseIf Test=\"Model = 'hello'\">" +
                            "<p>Hello</p>" +
                        "</ElseIf>" +
                        "<Else>" +
                            "<p>Goodbye</p>" +
                        "</Else>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run("hello");
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(3, section.Children.Count());
            Assert.IsType<If>(section.Children.First());
            Assert.IsType<ElseIf>(section.Children.ToList()[1]);
            Assert.IsType<Else>(section.Children.Last());
            Assert.Single(section.GetSectionModel().Elements);

            Assert.Equal("Hello", section.GetSectionModel().Elements.OfType<MigraDoc.DocumentObjectModel.Paragraph>().First().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
        }
    }
}
