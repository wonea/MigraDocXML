﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public abstract class SchemaType
    {

        public string Name { get; private set; }

        public SchemaType(string name)
        {
            Name = name;
        }

    }
}
